<!--
SPDX-FileCopyrightText: 2019 Freemelt AB <opensource@freemelt.com>

SPDX-License-Identifier: GPL-3.0-only
-->

# Metrics Relaying from MQTT to InfluxDB

This is a simple service that connects to an MQTT server and relays
messages to InfluxDB.

Instead configuring fields, tags and how they should be translated, we
establish a convention. The benefit is that you're faster up and
running when designing new services that need to report to MQTT and
which should be logged to InfluxDB. The drawbacks are for established
systems that already send data in a different format, and where other
solutions may be more appropriate.

MQTT topics follow this structure, which contains some parts that are
not yet used in InfluxDB, but which are good topic design:

`<product>/<pid>/<source>/<sid>/<measurement>/<tagname>/<tagvalue>`

* `product`: Unused placeholder for now. Can be the name of your
  product.
* `pid`: Unused placeholder for now. Can be the serial number of your
  product.
* `source`: This will be placed in the `Source` tag in InfluxDB.
* `sid`: Unused placeholder for now. This can identify the source when
  there are multiple instances of it.
* `measurement`: The name of the InfluxDB measurement.
* `tagname`: The name of the most significant tag.
* `tagvalue`: The value of that very same tag.

All points in InfluxDB will have a `Source` tag and one tag taken from
the topic. This can be any tag you have, but it needs to be something.
If you have multiple tags you want to use then it's better to think in
terms of MQTT topics and think about which tag you might want to
filter on in a subscription. If no tag is more important than another
tag, then you may as well pick one at random.

Payloads are JSON maps. They have this structure:

```
{"<Field>": <float|string|bool>,
 "<Number>_int": <float64>,
 "<AlsoNumber>_int": "<int64>",
 "time": "<unix_ns>",
 "_t": {"<tagname>": "<tagvalue>"}}
```

* `Field`: Any arbitrary key whose value is a float, string or bool
  will be sent as a field to InfluxDB.
* `*_int`: Any names ending with `_int` are reinterpreted as integers
  before being sent to InfluxDB. The value can be a float or string,
  as needed. This special case is needed because JSON does not have
  integers.
* `time`: This is the time of the sample in nanoseconds since the Unix
  epoch. By default the current time is used, which may be slightly
  off due to network and queuing delays.
* `_t`: This map, if present, is passed on to InfluxDB as tags.

Note that InfluxDB does not allow you to change the type of a field
once you have inserted any value on that field.
