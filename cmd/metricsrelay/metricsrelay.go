// SPDX-FileCopyrightText: 2019 Freemelt AB <opensource@freemelt.com>
//
// SPDX-License-Identifier: GPL-3.0-only

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"
	"time"

	"compress/gzip"
	"os"
	"os/signal"
	"path/filepath"
	"syscall"

	mqtt "github.com/eclipse/paho.mqtt.golang"
	influx "github.com/influxdata/influxdb/client/v2"
)

var mqttUrl = flag.String("mqtt", "tcp://localhost:1883", "MQTT server URL")
var influxUrl = flag.String("influx", "localhost:8089", "InfluxDB URL (HTTP URL or UDP)")
var influxDb = flag.String("database", "onelog", "InfluxDB database name")
var mqttTopic = flag.String("topic", "+/+/+/+/+/+/+", "Topic to subscribe to")
var verbose = flag.Bool("verbose", false, "Verbose output for debugging")
var enableArchive = flag.Bool("archive", false, "Enable archiving of MQTT messages to the filesystem")
var archiveFormat = flag.String("archive-format", "gzip", "Compression format for archived messages (currently only 'gzip' is supported)")

type MetricsConfig struct {
	MqttUrl        string
	MqttTopic      string
	InfluxUrl      string
	InfluxUsername string
	InfluxPassword string
	InfluxDatabase string
}

type MetricsRelay struct {
	cfg         MetricsConfig
	mqttClient  mqtt.Client
	msgCh       chan mqtt.Message
	archiver    *MessageArchiver
	batchBuffer []influx.BatchPoints
	maxBuffer   int
}

type MessageArchiver struct {
	baseDir      string
	format       string
	currentFile  *os.File
	gzWriter     *gzip.Writer
	interval     time.Duration
	nextRotation time.Time
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func NewMessageArchiver(format string) (*MessageArchiver, error) {
	baseDir := os.Getenv("LOGS_DIRECTORY")
	if baseDir == "" {
		baseDir = "/var/log/freemelt/metricsrelay"
	}
	mqttDir := filepath.Join(baseDir, "mqtt")

	if err := os.MkdirAll(mqttDir, 0755); err != nil {
		return nil, fmt.Errorf("failed to create mqtt directory: %v", err)
	}

	ma := &MessageArchiver{
		baseDir:  mqttDir,
		format:   format,
		interval: 8 * time.Hour,
	}

	if err := ma.rotateIfNeeded(); err != nil {
		return nil, err
	}

	return ma, nil
}

func NewMetricsRelay(mc *MetricsConfig) (mr *MetricsRelay) {
	// Get an MQTT connection
	opts := mqtt.NewClientOptions()
	opts.AddBroker(mc.MqttUrl)
	// Client ID must be unique not longer than 23 characters.
	// len("metricsrelay-4294967295") == 23
	opts.SetClientID(fmt.Sprintf("metricsrelay-%d", rand.Uint32()))
	opts.SetKeepAlive(2 * time.Second)
	opts.SetPingTimeout(1 * time.Second)
	opts.SetAutoReconnect(true)

	msgCh := make(chan mqtt.Message)
	opts.SetDefaultPublishHandler(func(client mqtt.Client, msg mqtt.Message) {
		msgCh <- msg
	})
	// opts.SetOnConnectionLostHandler(func(client mqtt.Client) {
	// 	log.Printf("Lost server connection")
	// })
	// opts.SetOnReconnectingHandler(func(client mqtt.Client, opts *mqtt.ClientOptions) {
	// 	log.Printf("Reconnecting...")
	// })
	opts.SetOnConnectHandler(func(client mqtt.Client) {
		log.Printf("Connected to MQTT broker %s", mc.MqttUrl)
		if token := client.Subscribe(mc.MqttTopic, 1, nil); token.Wait() && token.Error() != nil {
			log.Fatalf("MQTT: %v", token.Error())
		}
	})

	c := mqtt.NewClient(opts)
	if token := c.Connect(); token.Wait() && token.Error() != nil {
		log.Fatalf("MQTT: %v", token.Error())
	}

	var archiver *MessageArchiver
	if *enableArchive {
		var err error
		archiver, err = NewMessageArchiver(*archiveFormat)
		if err != nil {
			log.Fatalf("Failed to initialize archiver: %v", err)
			archiver = nil
		}
	}

	mr = &MetricsRelay{
		cfg:         *mc,
		mqttClient:  c,
		msgCh:       msgCh,
		archiver:    archiver,
		batchBuffer: []influx.BatchPoints{},
		maxBuffer:   10,
	}

	return mr
}

func (ma *MessageArchiver) rotateIfNeeded() error {
	now := time.Now()
	if ma.currentFile != nil && now.Before(ma.nextRotation) {
		return nil
	}

	// Close existing file if open
	if ma.gzWriter != nil {
		ma.gzWriter.Close()
		ma.currentFile.Close()
	}

	// Calculate rotation boundaries
	interval := ma.interval
	current := now.Truncate(interval)
	ma.nextRotation = current.Add(interval)

	// Open new file
	filename := filepath.Join(ma.baseDir, fmt.Sprintf("%s-mqtt.txt.gz", current.Format("2006-01-02_150405")))

	// Check if file exists and can be appended to
	var file *os.File
	if _, err := os.Stat(filename); err == nil {
		file, err = os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			return fmt.Errorf("failed to open log file %s: %v", filename, err)
		}
		log.Printf("Appending to existing log file %s", filename)
	} else {
		file, err = os.Create(filename)
		if err != nil {
			return fmt.Errorf("failed to create log file %s: %v", filename, err)
		}
		log.Printf("Writing to new log file %s", filename)
	}
	ma.currentFile = file
	ma.gzWriter = gzip.NewWriter(file)
	return nil
}

func (ma *MessageArchiver) Write(topic string, payload []byte) error {
	if err := ma.rotateIfNeeded(); err != nil {
		return err
	}

	_, err := fmt.Fprintf(ma.gzWriter, "%s %s\n", topic, string(payload))
	return err
}

func (ma *MessageArchiver) Close() error {
	if ma.gzWriter != nil {
		if err := ma.gzWriter.Close(); err != nil {
			return err
		}
	}
	if ma.currentFile != nil {
		return ma.currentFile.Close()
	}
	return nil
}

func (mr *MetricsRelay) relayRoutine() {
	var bp influx.BatchPoints
	cfg := influx.BatchPointsConfig{
		Database:  mr.cfg.InfluxDatabase,
		Precision: "ms",
	}

	ticker := time.NewTicker(1 * time.Second)

	// Add signal handling
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		<-sigChan
		if mr.archiver != nil {
			mr.archiver.Close()
		}
		os.Exit(0)
	}()

	for {
		if bp == nil {
			var err error

			if bp, err = influx.NewBatchPoints(cfg); err != nil {
				log.Fatalf("could not create batch points: %v", err)
			}
		}

		select {
		case <-ticker.C:
			mr.insertData(bp)
			bp = nil

		case msg := <-mr.msgCh:
			if *verbose {
				log.Printf("Received: %v", msg)
			}

			if msg.Retained() {
				continue
			}

			// Archive message if enabled
			if mr.archiver != nil {
				if err := mr.archiver.Write(msg.Topic(), msg.Payload()); err != nil {
					log.Printf("Failed to archive message: %v", err)
				}
			}

			pt, err := translateData(msg)
			if err != nil {
				log.Printf("could not create point: %v", err)
				continue
			}
			if pt != nil {
				if *verbose {
					log.Printf("Translated: %v %v\n", pt, err)
				}
				bp.AddPoint(pt)
			}
		}
	}
}

func translateData(mqttMsg mqtt.Message) (pt *influx.Point, err error) {
	/*
	* Topic:
	* product/<pid>/<source>/<sid>/<measurement>/<tagname>/<tagvalue>
	*
	* Payload is always JSON:
	* {"<Field>": <float|string|bool>,
	*  "<Number>_int": <float>,
	*  "<AlsoNumber>_int": "<int64>",
	*  "time": "<unix_ns>",
	*  "_t": {"<tagname>": "<tagvalue>"}}
	 */

	// Decode the relevant parts from the topic. Some are there
	// just because they are good topic design and aren't used
	// today.
	parts := strings.Split(mqttMsg.Topic(), "/")
	if len(parts) < 7 {
		return nil, nil
	}
	measurement := parts[4]
	tags := map[string]string{
		"PID":    parts[1],
		"Source": parts[2],
		parts[5]: parts[6],
	}

	// Decode the payload, which contains the time and optionally
	// fields and more tags.
	var e interface{}
	if err = json.Unmarshal(mqttMsg.Payload(), &e); err != nil {
		return
	}

	// Translate the JSON structure to InfluxDB fields and tags
	msg := e.(map[string]interface{})
	t := time.Now()
	fields := map[string]interface{}{}
	for k, v := range msg {
		switch k {
		case "time":
			// Extract the time of the sample
			switch vv := v.(type) {
			case string:
				var unixNs int64
				if unixNs, err = strconv.ParseInt(vv, 10, 64); err != nil {
					return
				}
				t = time.Unix(0, unixNs)
				continue

			default:
				log.Printf("dropping message with malformed time in payload=%s from topic=%s",
					string(mqttMsg.Payload()), mqttMsg.Topic())
				return
			}

		case "_t":
			msgTags, ok := v.(map[string]interface{})
			if !ok {
				log.Printf("dropping malformed _t in payload=%s from topic=%s",
					string(mqttMsg.Payload()), mqttMsg.Topic())
				continue
			}
			for tk, tv := range msgTags {
				switch tvv := tv.(type) {
				case string:
					tags[tk] = tvv
				default:
					log.Printf("dropping malformed tag value %v=%v in payload=%s from topic=%s",
						tk, tv, string(mqttMsg.Payload()), mqttMsg.Topic())

				}
			}

		default:

			switch vv := v.(type) {
			case nil:
				// Silently drop nil values
				continue

			case string:
				if strings.HasSuffix(k, "_int") {
					// Ints can be sent as strings in case
					// the float64 precision is not high
					// enough (see the similar case below)
					var intVal int64

					fieldname := k[0 : len(k)-len("_int")]
					if intVal, err = strconv.ParseInt(vv, 10, 64); err != nil {
						return
					}
					fields[fieldname] = intVal
				} else {
					fields[k] = vv
				}

			case float64:
				if strings.HasSuffix(k, "_int") {
					// Ints are a different datatype from
					// floats in InfluxDB, but not in JSON
					fieldname := k[0 : len(k)-len("_int")]
					fields[fieldname] = int64(vv)
				} else {
					fields[k] = vv
				}

			case bool:
				fields[k] = vv

			case map[string]interface{}:
				// Silently drop sub-dictionaries

			case []interface{}:
				// Silently drop arrays

			default:
				log.Printf("dropping malformed field %s=%v in payload=%s from topic=%s",
					k, vv, string(mqttMsg.Payload()), mqttMsg.Topic())
			}
		}
	}

	pt, err = influx.NewPoint(measurement, tags, fields, t)
	return
}

func (mr *MetricsRelay) insertData(bp influx.BatchPoints) {
	// Add current batch to buffer
	if len(mr.batchBuffer) >= mr.maxBuffer {
		mr.batchBuffer = append(mr.batchBuffer[1:], bp)
	} else {
		mr.batchBuffer = append(mr.batchBuffer, bp)
	}

	// Connect to InfluxDB
	var influxConn influx.Client
	var err error
	if strings.HasPrefix(mr.cfg.InfluxUrl, "http") {
		influxConn, err = influx.NewHTTPClient(influx.HTTPConfig{
			Addr:     mr.cfg.InfluxUrl,
			Username: mr.cfg.InfluxUsername,
			Password: mr.cfg.InfluxPassword,
		})
	} else {
		influxConn, err = influx.NewUDPClient(influx.UDPConfig{
			Addr: mr.cfg.InfluxUrl,
		})
	}
	if err != nil {
		log.Printf("failed to dial InfluxDB: %v", err)
		return
	}
	defer influxConn.Close()

	// Process all batch points from oldest to newest
	for i, bufferedBp := range mr.batchBuffer {
		if err := influxConn.Write(bufferedBp); err != nil {
			log.Printf("could not write batch: %v", err)
			mr.batchBuffer = mr.batchBuffer[i:]
			return
		}
	}
	mr.batchBuffer = nil
}

func main() {
	flag.Parse()

	if *enableArchive && *archiveFormat != "gzip" {
		log.Fatalf("Unsupported archive format '%s'. Currently only 'gzip' is supported.", *archiveFormat)
	}

	mc := &MetricsConfig{
		MqttUrl:        *mqttUrl,
		MqttTopic:      *mqttTopic,
		InfluxUrl:      *influxUrl,
		InfluxDatabase: *influxDb,
	}
	mr := NewMetricsRelay(mc)
	mr.relayRoutine()
}
